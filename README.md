### ** PLEASE READ README FOR INSTRUCTION OF USAGE OF ANSIBLE PLAYBOOK **
### **Ansible Playbook for Gather Information About VM's in VMWare vCenter**
### This Playbook can be used as a base or an exmaple to build other playbooks
### Requirements for Running Playbook on Windows 10
#### Make sure you have Git for Windows Install If you are using Windows. Located Here https://git-scm.com/download/win
#### If you are using Windows 10 make sure you have the Linux Subsystem Installled
#### Instructions for Installing Windows 10 Linux Subsystem. Located Here https://docs.microsoft.com/en-us/windows/wsl/install-win10
#### Install Ansible inside the Windows Linux Subsystem. Follow the Ubuntu Instructions for Installing Ansible in the Windows 10 Linux Subsystem
#### http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-ubuntu

## ** Instruction of Usage **
#### 1. Open the the Linux Subsystem Bash Terminal 
#### 2. Create a folder for code -  _mkdir playbook_code_
#### 3. cd to folder -  _cd playbook_code_
#### 4.Clone Playbook from BitBucket
#### ** git clone https://wilsoncharlesr@bitbucket.org/wilsoncharlesr/devops_vcenter_facts.git**
#### 6. Edit Ansible vault Variables - (_NOTE in order to do this you will need to decrypt the ansible vault in the group_var folder_)
#### 7. run this command to decrypt the vault - _ansible-vault decrypt vault.yml_
#### 8. run this command to encrypt the vault - _ansible-vault encrypt vault.yml_
#### **Run the Playbook from the Playbook Folder**
#### _ansible-playbook -vvvv devops_vc_facts.yml -check --ask-vault-pass_
####